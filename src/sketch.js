const squareSize = 40
const squares = []
const fields = []
let squareColor
let backdrop

function setup() {
  createCanvas(windowWidth, windowHeight)
  rectMode(CENTER)
  colorMode(HSB, 360, 100, 100)
  noStroke()

  squareColor = color(0)

  let palette = []
  for (let i = 0; i < 4; i++) {
    const c = color(random(360), random(80, 100), 80)
    palette.push(c)
  }

  backdrop = createGraphics(windowWidth, windowHeight)
  backdrop.background(0)
  backdrop.noStroke()

  for (let i = 0; i < 40; i++) {
    let c = palette[int(random(palette.length))]
    backdrop.fill(c)
    backdrop.ellipse(random(width), random(height), random(40, 200))
  }

  let nx = width / squareSize
  let ny = height / squareSize

  for (let i = 0; i < nx; i++) {
    for (let j = 0; j < ny; j++) {
      let xPos = i * squareSize + squareSize / 2
      let yPos = j * squareSize + squareSize / 2
      let square = new Square(xPos, yPos)
      squares.push(square)
    }
  }

  for (let i = 0; i < 6; i++) {
    let field = new Field()
    fields.push(field)

  }
}

function draw() {
  background(0)
  image(backdrop, 0, 0, width, height)
  for (const square of squares) {
    square.update()
    square.display()
  }
  for (const field of fields) {
    field.update()
    // field.display()
  }
}

class Field {
  constructor() {
    this.radius = 150
    this.location = createVector(random(width), random(height))
    this.speed = createVector(random(-3, 3), random(-3, 3))
  }

  update() {
    this.location.add(this.speed)

    if (this.location.x > width + this.radius / 2) {
      this.location.x = 0 - this.radius / 2;
    }

    if (this.location.x < 0 - this.radius / 2) {
      this.location.x = width + this.radius / 2;
    }

    if(this.location.y > height + this.radius / 2) {
      this.location.y = 0 - this.radius / 2
    }

    if (this.location.y < 0 - this.radius / 2) {
      this.location.y = height + this.radius/2
    }
  }

  display() {
    ellipse(this.location.x, this.location.y, this.radius * 2)
  }

  inRadius(location) {
    let d = dist(this.location.x, this.location.y, location.x, location.y)
    if (d <= this.radius) {
      return true
    } else {
      return false
    }
  }
}

class Square {
  constructor(x, y) {
    this.location = createVector(x, y)
    this.scaleFactor = 0
    this.c = squareColor
  }

  update() {
    for (const field of fields) {
      if (field.inRadius(this.location)) {
        this.scaleFactor += 1 - dist(
          this.location.x,
          this.location.y,
          field.location.x,
          field.location.y
        ) / field.radius
      }

      // if (field.inRadius(this.location)) {
      //   this.c = color(random(255, random(255, random(255))))
      //   break
      // } else {
      //   this.c = color(255)
      // }
    }
  }

  display() {
    fill(this.c)
    if (this.scaleFactor != 0) {
      stroke(color(200, 20, 50))
    } else {
      noStroke()
    }
    rect(
      this.location.x,
      this.location.y,
      squareSize * (1- this.scaleFactor)
    )

    this.scaleFactor = 0
  }
}
